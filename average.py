import csv
import math

def average(filename):
    file = open(filename)
    reader = csv.reader(file)
    header = []
    header = next(reader)
    rows = []
    for row in reader:
        rows.append(row)

    weight_list = []
    height_list = []

    for row in rows:
        weight_list.append(float(row[2]))
        height_list.append(float(row[1]))

    weight_avg = sum(weight_list)/len(weight_list)
    height_avg =  sum(height_list)/len(height_list)

    x_weight_list = []

    for el in weight_list:
        diff = (el - weight_avg)**2
        x_weight_list.append(diff)

    D_weight = sum(x_weight_list)/len(x_weight_list)
    weight_otclon = math.sqrt(D_weight)


    x_height_list = []

    for el in height_list:
        diff = (el - height_avg) ** 2
        x_height_list.append(diff)

    D_height = sum(x_height_list)/len(x_height_list)
    height_otclon = math.sqrt(D_height)

    print("height_avg:",height_avg)
    print("weight_avg:", weight_avg)
    print("height_otclon",height_otclon)
    print("weight_otclon", weight_otclon)


average('hw.csv')