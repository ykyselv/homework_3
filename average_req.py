import csv
import math
import requests
import re

def average(filename):
    URL = 'https://raw.githubusercontent.com/varelaz/python-course/master/03-git/hw.csv'
    res = requests.get(URL, headers={'User-Agent': 'Mozilla/5.0'}).text
    result = res.replace("\n",",")
    list_tmp = result.split(',')
    list_res = []

    j = 3
    for el in range(len(list_tmp) - 4):
        list_res.append(list_tmp[j])
        j += 1

    weight_list = []
    height_list = []
    i = 1
    for el in range(len(list_res)//3):
        height_list.append(float(list_res[i]))
        i = i + 3

    i = 2
    for el in range(len(list_res) // 3):
        weight_list.append(float(list_res[i]))
        i = i + 3

    weight_avg = sum(weight_list)/len(weight_list)
    height_avg =  sum(height_list)/len(height_list)

    x_weight_list = []

    for el in weight_list:
        diff = (el - weight_avg)**2
        x_weight_list.append(diff)

    D_weight = sum(x_weight_list)/len(x_weight_list)
    weight_otclon = math.sqrt(D_weight)

    x_height_list = []

    for el in height_list:
        diff = (el - height_avg) ** 2
        x_height_list.append(diff)

    D_height = sum(x_height_list)/len(x_height_list)
    height_otclon = math.sqrt(D_height)

    print("height_avg:",height_avg)
    print("weight_avg:", weight_avg)
    print("height_otclon",height_otclon)
    print("weight_otclon", weight_otclon)

average('hw.csv')